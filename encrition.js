var path = require("path");
var express = require("express");

var logger = require("morgan");
var bodyParser = require("body-parser"); // simplifies access to request body

var app = express();  // make express app
var http = require('http').Server(app);  // inject app into the server

app.use(express.static(__dirname + '/assets'));
//app.use(express.static(__dirname + '/views'));

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// 2 create an array to manage our entries
var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.render("Sherpa_Lhakpa");
});
app.get("/Sherpa_Lhakpa", function (request, response) {
  response.render("Sherpa_Lhakpa");
});
app.get("/about", function (request, response) {
  response.render("Sherpa_Lhakpa");
});
app.get("/calculator", function (request, response) {
  response.render("Calculator");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/guestbook", function (request, response) {
  response.render("index");
});
app.get("/contact", function (request, response) {
  response.render("Contact");
});
app.post("/contact", function(request, response){
  var api_key = 'key-c0ec147ac435d0a6324f5ef46796ceb9';
  var domain = 'sandbox7a0964a60b8441a488d8c087655e8a0d.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Mail Gun TustDady <postmaster@sandbox7a0964a60b8441a488d8c087655e8a0d.mailgun.org>',
    to: 'lhakpasb0000@gmail.com',
    subject: request.body.userEmail,
    text: request.body.body
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error){
      //response.send("Mail Sent");
      for(i=0; i<20; i++){     
        console.log("Your messsage has been sent");
      }
      response.redirect("/contact"); 
    }
    else{
      //alert("Please try again");
    response.send("Mail not sent");
    }
  });
});
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");  // where to go next? Let's go to the home page :)
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('A03 project listening on http://127.0.0.1:8081/');
});
